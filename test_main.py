from pytest import fixture
import csv, os
from main import create_character, find_character_by_id, find_all_characters, delete_character, update_character

filename = "characters.csv"

@fixture
def create_and_delete_file():
    with open(filename, 'w') as file:
        print("")
    yield('')
    if os.path.exists(filename):
        os.remove(filename)

def test_create_character(create_and_delete_file):
    name = "Huck"
    intelligence = '0'
    power = '7'
    strength = '10'
    agility = '8'

    expected = {
        "id": '1',
        "name": "Huck",
        "intelligence": '0',
        "power": '7',
        "strength": '10',
        "agility": '8'
    }
    result = create_character(filename, name, intelligence, power, strength, agility)
    assert result == expected

def test_find_character_by_id(create_and_delete_file):
    name = "Huck"
    intelligence = '0'
    power = '7'
    strength = '10'
    agility = '8'

    expected = {
        "id": '1',
        "name": "Huck",
        "intelligence": '0',
        "power": '7',
        "strength": '10',
        "agility": '8'
    }

    create_character(filename, name, intelligence, power, strength, agility)

    character_id = '1'
    result = find_character_by_id(filename, character_id) 
    assert result == expected 

def test_find_all_characters(create_and_delete_file):
    name = "Huck"
    intelligence = '0'
    power = '7'
    strength = '10'
    agility = '8'

    create_character(filename, name, intelligence, power, strength, agility)

    expected = [{
    "id": '1',
    "name": "Huck",
    "intelligence": '0',
    "power": '7',
    "strength": '10',
    "agility": '8'
    }]

    result = find_all_characters(filename)
    assert result == expected

def test_delete_character(create_and_delete_file):
    name = "Huck"
    intelligence = '0'
    power = '7'
    strength = '10'
    agility = '8'
    create_character(filename, name, intelligence, power, strength, agility)
    
    character_id = 1
    expected = True
    result = delete_character(filename, character_id)
    assert result == expected

def test_update_character(create_and_delete_file):
    name = "Huck"
    intelligence = '0'
    power = '7'
    strength = '10'
    agility = '8'
    create_character(filename, name, intelligence, power, strength, agility)
   
    character_id = 1
    to_update = {"power": 10, "intelligence": 10, "ameba": 10}
    
    expected = {
    "id": '1',
    "name": "Huck",
    "intelligence": '10',
    "power": '10',
    "strength": '10',
    "agility": '8'
    }

    result = update_character(filename, character_id, **to_update)
    assert result == expected
